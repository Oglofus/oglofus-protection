# OglofusProtection [![Build Status](https://travis-ci.org/Oglofus/OglofusProtection.svg)](https://travis-ci.org/Oglofus/OglofusProtection)

The OglofusProtection project is a free open source plugin for the Spigot/Bukkit and Sponge API. This plugin allows a player to place a special block, and this protects their area from griefing.

## Prerequisites
* [Java] 7
* [Maven] 2.1+

## Cloning
The following steps will ensure your project is cloned properly.

1. `git clone git@github.com:Oglofus/OglofusProtection.git`

## Compiling
Use Maven to compile OglofusProtection.

Run the following in your terminal/command prompt:
```shell
mvn clean package
```

## Download
https://github.com/Oglofus/OglofusProtection/releases
